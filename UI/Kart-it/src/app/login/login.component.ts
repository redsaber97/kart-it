import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { Router, Routes, ActivatedRoute } from '@angular/router';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

import { LoginService } from '../services/login.service';

//import { NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  //isTrue: boolean= false;
  //isTrue: boolean= false;
  //isTrue: boolean= false;
  //isTrue: boolean= false;
  SignupForm!: FormGroup;
  public data: any;
  alertmessage: string = '';

  images = [
    'assets/image/Slider_image01.jpg',
    'assets/image/Slider_image02.jpg',
    'assets/image/Slider_image03.jpg'
  ];

  @ViewChild('username')
  uname!: ElementRef;
  @ViewChild('password')
  pass!: ElementRef;
  // router: any;
  constructor(private loginService: LoginService, private router: Router){}
  ngOnInit() {
    this.initForm();
  }
  /*changeTheme(){
    this.isTrue = !this.isTrue;
  }*/

  initForm(){
    this.SignupForm = new FormGroup({
      'userData': new FormGroup({
      'username': new FormControl('',[Validators.required]),
      'password': new FormControl('',[Validators.required])
      })
    });
  }
  close() {
    this.alertmessage= "";
  }
  // loginProcess(){
  //   let username = this.uname?.nativeElement.value;
  //   let password = this.pass?.nativeElement.value;
  //   if(this.SignupForm.valid){
  //     this.loginService.getJSON().subscribe(data=>{
  //       console.log(Response);
  //       this.data =data;
  //       console.log(this.data);
  //       if(this.SignupForm.value in data){
  //         if('password' == password){
  //           console.log("Success");
  //           this.router.navigateByUrl('');
  //         }else{
  //           console.log("fail");
  //         }
  //       }
  //     });
  //   }
  // }
  onSubmit() {
    // this.loginService.getPeople()
    let userName = this.uname.nativeElement.value;
    let password = this.pass.nativeElement.value;
    this.loginService.getJSON().subscribe(data => {
      console.log(Response);
      this.data = data;
      console.log(this.data);
      if (userName in data){
        if ( this.data[userName]['password'] == password){
          //console.log("success");
          this.loginService.setUserInitials((this.data[userName].firstName.charAt(0) + this.data[userName].lastName.charAt(0)).toUpperCase());
          // console.log(this.login_service.getUserInitials());
          this.router.navigateByUrl('landing');
        }
        else{
          this.alertmessage = "Wrong credentials! Please try again!";
        }
      }
      else{
        this.alertmessage = "You are not registered in our system!";
      }
    });
   }
  
}