import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { baseUrl } from 'src/environments/environment';
//import * as data from '../../assets/mock_response/user.json';

// import { User } from './user';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  private userInitials: string = '';
  constructor(private http: HttpClient) { }

  login(data: any):Observable<any>{
      return this.http.post(`${baseUrl}users/login`,data);
  }

  public getJSON(){
    //return data[0];
   return this.http.get('../../assets/mock_response/user.json');
  }

  public getData(){}
  public getUserInitials():string {
    return this.userInitials;
  }
  public setUserInitials(userInitial: any) {
    this.userInitials = userInitial;
    localStorage.setItem('userInitial', userInitial);
  }
}
