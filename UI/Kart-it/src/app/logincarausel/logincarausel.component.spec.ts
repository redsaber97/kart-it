import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LogincarauselComponent } from './logincarausel.component';

describe('LogincarauselComponent', () => {
  let component: LogincarauselComponent;
  let fixture: ComponentFixture<LogincarauselComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LogincarauselComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LogincarauselComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
