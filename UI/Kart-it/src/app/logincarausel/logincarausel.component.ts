import { Component, OnInit } from '@angular/core';
import { NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-logincarausel',
  templateUrl: './logincarausel.component.html',
  styleUrls: ['./logincarausel.component.css']
})
export class LogincarauselComponent implements OnInit {

  constructor(config: NgbCarouselConfig) {
    config.interval = 5000;
    config.wrap = false;
    config.keyboard = false;
    config.pauseOnHover = false;
    config.wrap = true;
   }


  ngOnInit(): void {
  }

}
