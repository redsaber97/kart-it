
const express = require('express'); //import express

const router  = express.Router(); 

const teaController = require('../controllers/login'); 

router.get('/tea', teaController.newTea); 
module.exports = router; // export to use in server.js